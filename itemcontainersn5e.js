/**
 * This is your TypeScript entry file for Foundry VTT.
 * Register custom settings, sheets, and constants using the Foundry API.
 * Change this heading to be more descriptive to your module, or remove it.
 * Author: [your name]
 * Content License: [copyright and-or license] If using an existing system
 * 					you may want to put a (link to a) license or copyright
 * 					notice here (e.g. the OGL).
 * Software License: [your license] Put your desired license here, which
 * 					 determines how others may use and modify your module
 */
// Import JavaScript modules
// import { N5E } from '../../systems/n5e/module/config.js';
// import { ActorSheetN5e  } from '../../systems/n5e/module/actor/sheets/base.js';
// import { ActorSheetN5eCharacter  } from '../../systems/n5e/module/actor/sheets/character.js';
// import { ItemN5e } from '../../systems/n5e/module/item/entity.js';
// Import TypeScript modules
import { registerSettings } from './module/settings.js';
import { preloadTemplates } from './module/preloadTemplates.js';
import { ItemSheetN5eWithBags } from './module/ItemSheetN5eWithBags.js';
import { ItemSheetShop } from './module/ItemSheetShop.js';
import { fixupItemData, initHooks, readyHooks, setupHooks } from "./module/Hooks.js";
import { createJutsuListFromActor } from './module/util.js';
import { error, i18n, warn } from './module/libs/lib.js';
import CONSTANTS from './module/constants.js';
/* ------------------------------------ */
/* Initialize module					*/
/* ------------------------------------ */
Hooks.once('init', async () => {
    console.log(`${CONSTANTS.MODULE_NAME} | Initializing ${CONSTANTS.MODULE_NAME}`);
    Items.registerSheet("n5e", ItemSheetN5eWithBags, {
        makeDefault: true,
        types: ["backpack"],
        label: "ItemSheetN5eWithBags"
    });
    // Register custom module settings
    registerSettings();
    initHooks();
    // Preload Handlebars templates
    await preloadTemplates();
});
/* ------------------------------------ */
/* Setup module							*/
/* ------------------------------------ */
Hooks.once('setup', function () {
    // Do anything after initialization but before ready
    Items.registerSheet(game.system.id, ItemSheetN5eWithBags, { makeDefault: false, types: ["container"], label: i18n('itemcontainersn5e.ItemSheetN5eWithBags') });
    Items.registerSheet(game.system.id, ItemSheetShop, { makeDefault: false, types: ["container"], label: i18n('itemcontainersn5e.ItemSheetShop') });
    setupHooks();
});
/* ------------------------------------ */
/* When ready							*/
/* ------------------------------------ */
Hooks.once('ready', async () => {
    // Do anything once the module is ready
    if (!game.modules.get("lib-wrapper")?.active && game.user?.isGM) {
        error(`The '${CONSTANTS.MODULE_NAME}' module requires to install and activate the 'libWrapper' module.`, true);
        return;
    }
    readyHooks();
    window.ItemContainersN5e = {
        migrateItems,
        migrateActorItems,
        migrateAllActorItems,
        migrateAllTokenItems,
        migrateAllItems,
        migrateWorld,
        Shops: {
            createShop,
            createShopItem
        },
        createJutsuListFromActor: createJutsuListFromActor
    };
});
export async function migrateItems(items, name = "") {
    const promises = [];
    for (const item of items) {
        if (item.type === "container"
            && item.flags?.itemcontainersn5e
            && item.getFlag(CONSTANTS.MODULE_NAME, 'version') !== "0.8.6") {
            error(`Migrating ${name}: Item ${item.name}`);
            const itemcontents = duplicate(item.getFlag(CONSTANTS.MODULE_NAME, 'contents') || []);
            for (const itemData of itemcontents) {
                if (itemData.type === "container")
                    fixupItemData(itemData);
                (itemData.effects ?? []).forEach(effectData => {
                    effectData.origin = undefined;
                });
                itemData._id = randomID();
            }
            promises.push(item.update({
                "flags.itemcontainersn5e.version": "0.8.6",
                "flags.itemcontainersn5e.bagBulk": item.flags.itemcontainersn5e?.fixedBulk ?? 0,
                "flags.itemcontainersn5e.bagPrice": item.system.price,
                "flags.itemcontainersn5e.contentsData": itemcontents,
                "flags.itemcontainersn5e.-=contents": null,
                "flags.itemcontainersn5e.-=ryoValue": null,
                "flags.itemcontainersn5e.-=fixedBulk": null,
                "flags.itemcontainersn5e.-=importJutsu": null
            }));
            // promises.push(item.update({"flags.itemcontainersn5e.-=contents": null, "flags.itemcontainersn5e.-=fixedBulk": null}));
        }
    }
    return Promise.all(promises);
}
async function migrateActorItems(actor) {
    if (!(actor instanceof Actor)) {
        error(`${actor} is not an actor`);
        return;
    }
    return migrateItems(actor.items.contents, actor.name);
}
async function migrateAllActorItems() {
    const promises = [];
    for (const actor of game.actors) {
        promises.push(migrateActorItems(actor));
    }
    return Promise.all(promises);
}
async function migrateAllTokenItems() {
    const promises = [];
    for (const scene of game.scenes) {
        for (const tokenDocument of scene.tokens) {
            if (!tokenDocument.isLinked && tokenDocument.actor) {
                promises.push(migrateActorItems(tokenDocument.actor));
            }
        }
    }
    return Promise.all(promises);
}
async function migrateAllItems() {
    return migrateItems(game.items?.contents, "World");
}
async function migrateWorld() {
    Promise.all([migrateAllItems(), migrateAllActorItems(), migrateAllTokenItems()]);
}
export async function createShop(compendiumName, shopName, conditions, { minValue = 0, minQuantity = 0, createShop = false }) {
    const compendium = game.packs.get(compendiumName);
    if (!compendium) {
        warn(`Could not find compendium ${compendiumName}`, true);
        error(`Could not find compendium ${compendiumName}`);
        return;
    }
    let shop = game.items?.getName(shopName);
    if (createShop) {
        shop = await createShopItem(shopName);
        error(`Itemcontainers: Created shop shopName ${duplicate(shop.flags.itemcontainersn5e)}`);
    }
    else if (!shop || shop.type !== "container") {
        warn(`Could not find a shop named ${shopName}`, true);
        error(`Could not find a shop named ${shopName}`);
        return;
    }
    await compendium.getDocuments();
    const itemsToAdd = compendium.filter(item => chooseCondition(item, conditions))
        .map(item => item.toObject(false))
        .map(itemData => {
        itemData.system.quantity = Math.max(itemData.system.quantity || 0, minQuantity);
        return itemData;
    })
        .map(itemData => {
        if (itemData.system.price && minValue > (itemData.system.quantity * itemData.system.price)) {
            itemData.system.quantity = Math.ceil(minValue / itemData.system.price);
        }
        return itemData;
    });
    console.log(`Creating ${itemsToAdd.length} items, out of ${compendium.size} in ${shopName}`);
    await shop.createEmbeddedDocuments("Item", itemsToAdd);
    ui.notifications.notify(`Shop ${shopName} finished`);
}
export function chooseCondition(item, { rarity = "", type = "", consumableType = "", equipmentType = "", nameRegExp = null, maxPrice = 999999999999 }) {
    if (rarity && item.system.rarity !== rarity)
        return false;
    if (type && item.type !== type)
        return false;
    if (item.system.price > maxPrice)
        return false;
    if (item.type === "consumable" && consumableType && consumableType !== item.system.consumableType)
        return false;
    if (item.type === "equipment" && equipmentType && equipmentType !== item.system.armor.type)
        return false;
    if (nameRegExp && !item.name.match(nameRegExp))
        return false;
    return true;
}
export async function createShopItem(shopName) {
    return Item.create({
        name: shopName,
        type: "container",
        "flags.core.sheetClass": "n5e.ItemSheetShop",
        img: "icons/environment/settlement/market-stall.webp",
        "flags.itemcontainersn5e.contentsData": [],
        "flags.itemcontainersn5e.bagBulk": 0,
        "flags.itemcontainersn5e.bagPrice": 0,
        "flags.itemcontainersn5e.version": "0.8.6",
        "data.capacity.value": 0,
        "data.capacity.weightless": true,
    });
}