import { warn } from "./libs/lib.js";
import { getEmbeddedDocument, createEmbeddedDocuments, deleteEmbeddedDocuments, updateEmbeddedDocuments, prepareEmbeddedEntities, getEmbeddedCollection, _onCreateDocuments, calcPrice, calcBulk, containedItemCount, deleteDocuments, getActor, updateDocuments, calcItemBulk, _update, _delete, prepareDerivedData, isEmbedded, getChatData } from "./ItemContainer.js";
import CONSTANTS from "./constants.js";
import "./ItemSheetN5eWithBags.js";
export const readyHooks = async () => {
    warn("Ready Hooks processing");
};
export const initHooks = () => {
    warn("Init Hooks processing");
    // setup all the hooks
    libWrapper.register(CONSTANTS.MODULE_NAME, "CONFIG.Item.documentClass.prototype.getEmbeddedDocument", getEmbeddedDocument, "MIXED");
    libWrapper.register(CONSTANTS.MODULE_NAME, "CONFIG.Item.documentClass.prototype.createEmbeddedDocuments", createEmbeddedDocuments, "MIXED");
    libWrapper.register(CONSTANTS.MODULE_NAME, "CONFIG.Item.documentClass.prototype.deleteEmbeddedDocuments", deleteEmbeddedDocuments, "MIXED");
    libWrapper.register(CONSTANTS.MODULE_NAME, "CONFIG.Item.documentClass.prototype.updateEmbeddedDocuments", updateEmbeddedDocuments, "MIXED");
    libWrapper.register(CONSTANTS.MODULE_NAME, "CONFIG.Item.documentClass.prototype.prepareEmbeddedDocuments", prepareEmbeddedEntities, "WRAPPER");
    libWrapper.register(CONSTANTS.MODULE_NAME, "CONFIG.Item.documentClass.prototype.getEmbeddedCollection", getEmbeddedCollection, "MIXED");
    libWrapper.register(CONSTANTS.MODULE_NAME, "CONFIG.Item.documentClass.prototype.prepareDerivedData", prepareDerivedData, "WRAPPER");
    libWrapper.register(CONSTANTS.MODULE_NAME, "CONFIG.Item.documentClass.prototype.actor", getActor, "OVERRIDE");
    libWrapper.register(CONSTANTS.MODULE_NAME, "CONFIG.Item.documentClass.prototype.update", _update, "MIXED");
    libWrapper.register(CONSTANTS.MODULE_NAME, "CONFIG.Item.documentClass.prototype.delete", _delete, "MIXED");
    libWrapper.register(CONSTANTS.MODULE_NAME, "CONFIG.Item.documentClass.prototype.isEmbedded", isEmbedded, "OVERRIDE");
    libWrapper.register(CONSTANTS.MODULE_NAME, "CONFIG.Item.documentClass._onCreateDocuments", _onCreateDocuments, "MIXED");
    libWrapper.register(CONSTANTS.MODULE_NAME, "CONFIG.Item.documentClass.deleteDocuments", deleteDocuments, "MIXED");
    libWrapper.register(CONSTANTS.MODULE_NAME, "CONFIG.Item.documentClass.updateDocuments", updateDocuments, "MIXED");
    libWrapper.register(CONSTANTS.MODULE_NAME, "CONFIG.Item.documentClass.prototype.getChatData", getChatData, "WRAPPER");
    async function testOnDrop(wrapped, event, data) {
        const returnValue = await wrapped(event, data);
        if (data.uuid && returnValue) {
            const item = await fromUuid(data.uuid);
            if (item?.parent instanceof Item)
                await item.delete();
        }
        return returnValue;
    }
    
    libWrapper.register(CONSTANTS.MODULE_NAME, "ActorSheet.prototype._onDropItem", testOnDrop, "WRAPPER");
};
export const setupHooks = () => {
    warn("Setup Hooks processing");
    CONFIG.Item.documentClass.prototype.calcBulk = calcBulk;
    CONFIG.Item.documentClass.prototype.calcItemBulk = calcItemBulk;
    CONFIG.Item.documentClass.prototype.calcPrice = calcPrice;
    CONFIG.Item.documentClass.prototype.containedItemCount = containedItemCount;
    Hooks.on("preCreateItem", (candidate, data, options, user) => {
        if (!(candidate instanceof Item
            && candidate.type === "container"
            && data.flags?.itemcontainersn5e
            && candidate.getFlag(CONSTANTS.MODULE_NAME, 'version') !== "0.8.6"))
            return true;
        if (data.flags.itemcontainersn5e?.contents && data.flags.itemcontainersn5e?.version !== "0.8.6") { // old version to convert
            const itemcontainersn5eData = {
                contentsData: duplicate(data.flags.itemcontainersn5e.contents || []),
                version: "0.8.6",
                bagBulk: data.flags.itemcontainersn5e?.fixedBulk ?? 0,
                bagPrice: data.system.price ?? 0
            };
            itemcontainersn5eData.contentsData.forEach(itemData => {
                itemData._id = randomID();
                (itemData.effects ?? []).forEach(effectData => {
                    effectData.origin = undefined;
                });
                if (itemData.type === "container")
                    fixupItemData(itemData);
            });
             updateSource
            candidate.updateSource({
                "flags.itemcontainersn5e.-=contents": null,
                "flags.itemcontainersn5e.-=ryoValue": null,
                "flags.itemcontainersn5e.-=fixedBulk": null,
                "flags.itemcontainersn5e.-=importJutsu": null,
                "flags.itemcontainersn5e.-=itemBulk": null
            });
             updateSource
            candidate.updateSource({ "flags.itemcontainersn5e": itemcontainersn5eData });
        }
    });
    Hooks.on("updateItem", (item, updates, options, user) => {
    });
};
export function fixupItemData(itemData) {
    if (!itemData.flags.itemcontainersn5e || itemData.flags.itemcontainersn5e.version === "0.8.6")
        return;
    const itemcontents = duplicate(itemData.flags.itemcontainersn5e.contents || []);
    for (const iidata of itemcontents) {
        iidata._id = randomID();
        (iidata.effects ?? []).forEach(effectData => {
            effectData.origin = undefined;
        });
        if (iidata.type === "container")
            fixupItemData(iidata);
    }
    itemData.flags.itemcontainersn5e.version = "0.8.6";
    itemData.flags.itemcontainersn5e.bagBulk = itemData.flags.itemcontainersn5e?.fixedBulk ?? 0;
    itemData.flags.itemcontainersn5e.bagPrice = itemData.data.price ?? 0;
    itemData.flags.itemcontainersn5e.contentsData = itemcontents;
    delete itemData.flags.itemcontainersn5e.contents;
    delete itemData.flags.itemcontainersn5e.ryoValue;
    delete itemData.flags.itemcontainersn5e.fixedBulk;
    delete itemData.flags.itemcontainersn5e.importJutsu;
    delete itemData.flags.itemcontainersn5e.itemBulk;
}