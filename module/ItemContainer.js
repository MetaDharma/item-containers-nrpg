import "./ItemSheetN5eWithBags.js";
import "./ItemSheetShop.js";
import { displayDescription } from "./settings.js";
export function getActor() {
    if (this.parent instanceof Item)
        return null;
    return this.parent;
}
export async function createEmbeddedDocuments(wrapped, embeddedName, data, options) {
    if (this.type !== "container" || embeddedName !== "Item")
        return wrapped(embeddedName, data, options);
    if (!Array.isArray(data))
        data = [data];
    let currentItems = duplicate(getProperty(this, "flags.itemcontainersn5e.contentsData") ?? []);
    currentItems = currentItems.map(itemData => {
        if (itemData.data) {
            itemData.system = itemData.data;
            delete itemData.data;
        };
        return itemData;
    });
    if (data.length) {
        for (const itemData of data) {
            let theData = itemData;
            theData._id = randomID();
            // const theItem = new CONFIG.Item.documentClass(theData, {parent: this}).toJSON();
            currentItems.push(theData);
            Hooks.callAll("createItemData", itemData, options, game.user?.id);
        }
        if (this.parent)
            return await this.parent.updateEmbeddedDocuments("Item", [{ "_id": this.id, "flags.itemcontainersn5e.contentsData": currentItems }]);
        else
            return setCollection(this, currentItems);
    }
}
export function isEmbedded() {
    // for items with an item parent we need to relax the definition a bit.
    // TODO find out how to do this with proper wrapping
    if (!(this.parent instanceof Item))
        return (this.parent !== null) && (this.documentName in this.parent.constructor.metadata.embedded);
    return (this.parent !== null);
}
export async function createDocuments(wrapped, data = [], context = { parent: {}, pack: {}, options: {} }) {
    const { parent, pack, options } = context;
    if (!(this.type === "container" && parent instanceof Item))
        return wrapped(data, context);
    return await parent.createEmbeddedDocuments("Item", data, options);
}
export function getEmbeddedDocument(wrapped, embeddedName, id, { strict = false } = {}) {
    if (this.type !== "container")
        return wrapped(embeddedName, id, { strict });
    return this.items.get(id);
}
export async function updateEmbeddedDocuments(wrapped, embeddedName, data, options) {
    if (this.type !== "container" || embeddedName !== "Item")
        return wrapped(embeddedName, data, options);
    const contained = getProperty(this, "flags.itemcontainersn5e.contentsData") ?? [];
    if (!Array.isArray(data))
        data = [data];
    const updated = [];
    const newContained = contained.map(existing => {
        if (!existing.system && existing.data) {
            existing.system = existing.data;
            delete existing.data;
        }
        const theUpdate = data.find(update => update._id === existing._id);
        if (theUpdate) {
            const newData = mergeObject(existing, theUpdate, { overwrite: true, insertKeys: true, insertValues: true, inplace: false });
            updated.push(newData);
            return newData;
        }
        return existing;
    });
    if (updated.length > 0) {
        if (this.parent) {
            await setCollection(this, newContained);
            delete this.flags.itemcontainersn5e.contents;
            await this.parent.updateEmbeddedDocuments("Item", [{ "_id": this.id, "flags.itemcontainersn5e.contentsData": newContained }]);
        }
        else {
            await setCollection(this, newContained);
        }
    }
    return updated;
}
export async function updateDocuments(wrapped, updates = [], context = { parent: {}, pack: {}, options: {} }) {
    const { parent, pack, options } = context;
    // An item whose parent is an item only exists in the parents embedded documents
    if (!(parent instanceof Item && parent.type !== "container"))
        return wrapped(updates, context);
    return parent.updateEmbeddedDocuments("Item", updates, options);
}
async function setCollection(item, contents) {
    // item.update({"flags.itemcontainersn5e.contentsData": contents);
    const rv = await item.setFlag("itemcontainersn5e", "contentsData", duplicate(contents));
    return rv;
}
export async function deleteEmbeddedDocuments(wrapped, embeddedName, ids = [], options = {}) {
    if (this.type !== "container" || embeddedName !== "Item")
        return wrapped(embeddedName, ids, options);
    const containedItems = getProperty(this, "flags.itemcontainersn5e.contentsData") ?? [];
    const newContained = containedItems.filter((itemData) => !ids.includes(itemData._id));
    const deletedItems = this.items.filter((item) => ids.includes(item.id));
    if (this.parent) {
        // await setCollection(this, newContained);
        await this.parent.updateEmbeddedDocuments("Item", [
            { "_id": this.id, "flags.itemcontainersn5e.contentsData": newContained }
        ]);
    }
    else {
        await setCollection(this, newContained);
    }
    deletedItems.forEach(item => Hooks.callAll("deleteItem", item, options, game.user?.id));
    return deletedItems;
}
export async function deleteDocuments(wrapped, ids = [], context = { parent: {}, pack: {}, options: {} }) {
    const { parent, pack, options } = context;
    if (!(parent instanceof Item && parent.type === "container"))
        return wrapped(ids, context);
    // an Item whose parent is an item only exists in the embedded documents
    return parent.deleteEmbeddedDocuments("Item", ids);
}
export function getEmbeddedCollection(wrapped, type) {
    if (type === "Item" && this.type === "container")
        return this.items;
    return wrapped(type);
}
export function prepareDerivedData(wrapped) {
    wrapped();
    if (!(this instanceof Item && this.type === "container" && this.flags.itemcontainersn5e))
        return;
    if (this.flags?.core?.sheetClass !== "n5e.ItemSheetN5eWithBags" && this.flags?.core?.sheetClass !== "n5e.ItemSheetShop")
        return;
    // if (!(this.sheet instanceof ItemSheetN5eWithBags || this.sheet instanceof ItemSheetShop)) return;
    this.system.bulk = calcBulk.bind(this)();
    this._source.system.bulk = calcBulk.bind(this)();
    if (game.system.id === "n5e" && isNewerVersion(game.system.version, "2.0.3")) {
        if ((typeof this.system.price) !== "object") {
            console.error("unexpected price value ", this, this.system.price);
            this.system.price = {};
        }
        let thePrice = calcPrice.bind(this)();
        if (isNaN(thePrice))
            thePrice = 0;
        this.system.price.value = thePrice;
        this.system.price.denomination = "ryo";
        this._source.system.price.value = this.thePrice;
        this._source.system.price.denomination = "ryo";
    }
    else {
        this.system.price = calcPrice.bind(this)();
        this._source.system.price = calcPrice.bind(this)();
    }
    if (!this.system.details)
        setProperty(this.system, "details", {});
}
export function prepareActorItems(wrapped) {
    wrapped();
    for (let item of this.items) {
        if (this.type !== "container")
            continue;
        prepareEmbeddedEntities.bind(item)(undefined);
    }
}
export function prepareEmbeddedEntities(wrapped) {
    if (wrapped)
        wrapped();
    if (!(this instanceof Item && this.type === "container"))
        return;
    const containedItems = (getProperty(this, "flags.itemcontainersn5e.contentsData") ?? []).map(itemData => {
        if (!itemData.system && itemData.data) {
            itemData.system = itemData.data;
            delete itemData.data;
        };
        return itemData;
    });
    const oldItems = this.items;
    this.items = new foundry.utils.Collection();
    containedItems.forEach(idata => {
        if (!(oldItems?.has(idata._id))) {
            let theItem;
            try {
                theItem = new CONFIG.Item.documentClass(idata, { parent: this });
                this.items.set(idata._id, theItem);
            }
            catch (err) {
                console.error("itemcontainers | Create item error", idata, theItem, err);
            }
        }
        else { // TODO see how to avoid this - here to make sure the contained items is correctly setup
            const currentItem = oldItems.get(idata._id);
            currentItem.flags = mergeObject(currentItem.flags ?? {}, idata.flags);
            currentItem.system = mergeObject(currentItem.system ?? {}, idata.system);
            currentItem.updateSource(duplicate(idata));
            // setProperty(currentItem, "_source", idata);
            currentItem.name = idata.name;
            currentItem.img = idata.img;
            currentItem.prepareData();
            this.items.set(idata._id, currentItem);
            if (!isEmpty(CONFIG.Item.sheetClasses)) {
                if (this.sheet?.rendered) { // TODO once v11 only move this inside test above
                    this.system.bulk = calcBulk.bind(this)();
                    this.sheet.render(false, { action: "update", data: currentItem.toObject(false) });
                }
                if (currentItem.sheet?.rendered) {
                    currentItem.sheet.render(false, { action: "update", data: currentItem.toObject(false) });
                    // currentItem._sheet.render(false, {action: "update"});
                }
            }
        }
    });
}
export async function _update(wrapped, data, context) {
    if (!(this.parent instanceof Item))
        return wrapped(data, context);
    data = foundry.utils.expandObject(data);
    data._id = this.id;
    await this.parent.updateEmbeddedDocuments("Item", [data]);
    this.render(false, { action: "update", data: data });
}
export async function _delete(wrapped, data) {
    if (!(this.parent instanceof Item))
        return wrapped(data);
    return this.parent.deleteEmbeddedDocuments("Item", [this.id]);
}
export async function _onCreateDocuments(wrapped, items, context) {
    if (!(context.parent instanceof Item))
        return wrapped(items, context);
    if (items.filter(item => item.type === "container").length === 0)
        return wrapped(items, context);
    // if (!(context.parent instanceof Item && this.type === "container")) return wrapped(items, context);
    const toCreate = [];
    for (const item of items) {
        for (const e of item.effects) {
            if (!e.transfer)
                continue;
            const effectData = e.toObject();
            effectData.origin = item.uuid;
            toCreate.push(effectData);
        }
    }
    if (!toCreate.length)
        return [];
    const cls = getDocumentClass("ActiveEffect");
    return cls.createDocuments(toCreate, context);
}
export function calcBulk({ ignoreItems, ignoreTypes } = { ignoreItems: undefined, ignoreTypes: undefined }) {
    if (this.type !== "container" || !this.flags.itemcontainersn5e)
        return calcItemBulk.bind(this)();
    if (this.parent instanceof Actor && !this.system.equipped)
        return 0;
    const weightless = getProperty(this, "system.capacity.weightless") ?? false;
    if (weightless)
        return getProperty(this, "flags.itemcontainersn5e.bagBulk") ?? 0;
    return calcItemBulk.bind(this)({ ignoreItems, ignoreTypes }) + ((getProperty(this, "flags.itemcontainersn5e.bagBulk") || 0));
}
export function calcItemBulk({ ignoreItems, ignoreTypes } = { ignoreItems: [], ignoreTypes: [] }) {
    if (this.type !== "container" || this.items === undefined)
        return _calcItemBulk(this);
    const bulk = this.items.reduce((acc, item) => {
        if (ignoreTypes?.some(name => item.name.includes(name)))
            return acc;
        if (ignoreItems?.includes(item.name))
            return acc;
        return acc + (calcBulk.bind(item)() || 0);
    }, (this.type === "container" ? 0 : (_calcItemBulk(this)) || 0));
    return Math.round(bulk);
}
export function containedItemCount() {
    if (this.type !== "container")
        return (this.system.quantity ?? 1);
    return this.items.reduce((acc, item) => acc + item.containedItemCount(), 0);
}
export function _calcItemPrice(item) {
    if (item.type === "container")
        return item.flags.itemcontainersn5e?.bagPrice ?? 0;
    const quantity = item.system.quantity || 1;
    let price = item.system.price ?? 0;
    return Math.round(price * quantity * 100) / 100;
}
export function _calcItemBulk(item) {
    const quantity = item.system.quantity || 1;
    const bulk = item.system.bulk || 0;
    return Math.round(bulk * quantity * 100) / 100;
}
export function calcPrice() {
    if (this.type !== "container" || this.items === undefined)
        return _calcItemPrice(this);
    const currency = this.system.currency ?? {};
    const coinValue = currency ? Object.keys(currency)
        .reduce((val, denom) => val += {
        "ryo": 1
    }[denom] * currency[denom], 0) : 0;
    const price = this.items.reduce((acc, item) => acc + (calcPrice.bind(item)() ?? 0), _calcItemPrice(this) || 0);
    return Math.round((price + coinValue) * 100) / 100;
}
export async function getChatData(wrapped, ...args) {
    const chatData = await wrapped(...args);
    if (displayDescription || this.type !== "container" || this.items === undefined)
        return chatData;
    chatData.description.value = "<table>";
    for (const item of this.items) {
        let itemString = "";
        if (item.type === "container" && item.items !== undefined)
            itemString = `<tr><td>${item.name}</td><td>${(await item.getChatData()).description.value}</td></tr>`;
        else
            itemString = `<tr><td>${item.name}</td><td>${item.system.quantity ?? ""}</td><td>${item.system.bulk ?? ""}</td></tr>`;
        chatData.description.value += itemString;
    }
    chatData.description.value += "</table>";
    return chatData;
}