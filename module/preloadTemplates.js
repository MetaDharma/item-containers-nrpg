import CONSTANTS from "./constants.js";
export const preloadTemplates = async function () {
    // const CONSTANTS.MODULE_NAME = 'itemcontainersn5e';
    const templatePaths = [
        // Add paths to "module/XXX/templates"
        `modules/${CONSTANTS.MODULE_NAME}/templates/bag-sheet.hbs`,
        `modules/${CONSTANTS.MODULE_NAME}/templates/bag-description.hbs`,
        `modules/${CONSTANTS.MODULE_NAME}/templates/shop-sheet.hbs`
    ];
    return loadTemplates(templatePaths);
};