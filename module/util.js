export const Itemcontainersn5e = {
  async convertToContainer(itemName) {
      const item = game.items?.contents.find(i => i.name === itemName);
      if (!item) {
          ui.notifications.error(`Could not find ${itemName}`);
          return;
      }
      const capacity = { type: "bulk", value: 0, weightless: false };
      const currency = { ryo: 0 };
      await item.update({
          "type": "container",
          "system.capacity": capacity,
          "system.currency": currency
      });
      const sheet = item.sheet;
      if (item.sheet) {
          await sheet.close();
      }
      //item.sheet = null;
      delete item.apps[sheet.appId];
      // Update the Entity-specific override
      //  await item.setFlag("core", "sheetClass", "n5e.ItemSheetN5eWithBags");
  }
};
const jutsulistData = "{\"_id\":\"OQU13yuxA8yo5gCJ\",\"name\":\"Jutsulist IC\",\"type\":\"container\",\"img\":\"systems/n5e/icons/items/inventory/book-purple.jpg\",\"data\":{\"description\":{\"value\":\"<p><span style=\\\"color: #191813; font-size: 13px;\\\">Closely guarded by entire villages and passed down through the generations, a jutsulist is a powerful scroll that holds the teachings to master esoteric jutsu.</span></p>\",\"chat\":\"\",\"unidentified\":\"\",\"type\":\"String\",\"label\":\"Description\"},\"source\":\"\",\"quantity\":1,\"bulk\":3,\"price\":50,\"attunement\":0,\"equipped\":false,\"rarity\":\"common\",\"identified\":true,\"capacity\":{\"type\":\"bulk\",\"value\":0,\"weightless\":true},\"currency\":{\"ryo\":0}},\"effects\":[],\"folder\":\"3GwtOCdI8TJfIwxM\",\"sort\":0,\"permission\":{\"default\":0,\"g4WGw0lAZ3nIhapn\":3},\"flags\":{\"_sheetTab\":\"details\",\"itemcontainersn5e\":{\"version\":\"0.8.6\",\"bagBulk\":3,\"bagPrice\":50,\"contentsData\":[]},\"core\":{\"sheetClass\":\"n5e.ItemSheetN5eWithBags\",\"sourceId\":\"Compendium.itemcontainersn5e.packs.jUTpEHQg0psJKHbF\"},\"chakraitems\":{\"enabled\":false,\"equipped\":false,\"attuned\":false,\"charges\":\"0\",\"chargeType\":\"c1\",\"destroy\":false,\"destroyFlavorText\":\"reaches 0 charges: it crumbles into ashes and is destroyed.\",\"rechargeable\":false,\"recharge\":\"0\",\"rechargeType\":\"t1\",\"rechargeUnit\":\"r1\",\"sorting\":\"l\"}}}";
export async function createJutsuListFromActor(actor) {
  const jutsuListJson = JSON.parse(jutsulistData);
  jutsuListJson.name = `${game.i18n.localize("N5E.Jutsulist")} - ${actor.name}`;
  const itemsData = actor.items.filter(i => i.type === "jutsu").map(i => i.data);
  const theItem = await CONFIG.Item.documentClass.create(jutsuListJson);
  return theItem.createEmbeddedDocuments("Item", itemsData);
}